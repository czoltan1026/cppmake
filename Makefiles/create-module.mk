# $1 = Module name 
# $2 = List of module dependencies 
# $3 = extra compiler flags

define create-module

directory_$1          := $1

dependencies_$1 := $$(addsuffix .dll,$2)
dependencies_$1 := $$(subst src/,,$$(dependencies_$1))

dependencyIncludes_$1 := $$(addprefix -I,$2)
SelfIncludes_$1       := $$(addprefix -I,$$(directory_$1))

filesCache_$1  := build/$1_cache.txt
files_$1       := $$(file < $$(filesCache_$1))
ifeq ($$(files_$1),)
files_$1       := $$(shell mkdir -p build/src ; find $$(directory_$1) -name "*.cpp"  > $$(filesCache_$1) )
files_$1       := $$(file < $$(filesCache_$1))
endif

cfilesCache_$1  := build/$1_ccache.txt
cfiles_$1       := $$(file < $$(cfilesCache_$1))
ifeq ($$(files_$1),)
cfiles_$1       := $$(shell mkdir -p build/src ; find $$(directory_$1) -name "*.c"  > $$(cfilesCache_$1) )
cfiles_$1       := $$(file < $$(cfilesCache_$1))
endif

sources_$1 := $$(filter-out $$(directory_$1)/test/%.cpp,$$(files_$1))
csources_$1 := $$(filter-out $$(directory_$1)/test/%.c,$$(cfiles_$1))

objects_$1 := $$(addprefix  build/,$$(sources_$1))
objects_$1 := $$(addsuffix  .o,$$(objects_$1))

cobjects_$1 := $$(addprefix  build/,$$(csources_$1))
cobjects_$1 := $$(addsuffix  .o,$$(cobjects_$1))

lib_$1     := $$(addsuffix  .dll,$1)
lib_$1     := $$(subst      src/,,$$(lib_$1))

headerdependencies_$1 := $$(addsuffix  .d,$$(objects_$1) $$(cobjects_$1))

tests_$$(directory_$1)    := $$(filter $$(directory_$1)/test/%.cpp,$$(files_$$(directory_$1)))
testLibs_$$(directory_$1) := $$(addprefix build/,$$(tests_$$(directory_$1)))
testLibs_$$(directory_$1) := $$(addsuffix .dll,$$(testLibs_$$(directory_$1)))

CppFlags_$1           := $3


-include $$(headerdependencies_$1)
$$(objects_$$(directory_$1)) : build/$$(directory_$1)/%.cpp.o : $$(directory_$1)/%.cpp 
	@mkdir -p $$(@D)  
	g++ -c $$< -o $$@ $$(globalCppFlags) $$(CppFlags_$1) $$(SelfIncludes_$1) $$(dependencyIncludes_$1) -MMD -MF $$@.d


$$(cobjects_$$(directory_$1)) : build/$$(directory_$1)/%.c.o : $$(directory_$1)/%.c 
	@mkdir -p $$(@D)  
	gcc -c $$< -o $$@ $$(globalCppFlags) $$(CFlags_$1) $$(SelfIncludes_$1) $$(dependencyIncludes_$1)  -MMD -MF $$@.d

$$(lib_$$(directory_$1)) : $$(objects_$$(directory_$1)) $$(cobjects_$$(directory_$1)) | $$(dependencies_$1) $$(dlls)
	@mkdir -p $$(@D)
	g++ -shared $$^ $$| -o $$@ $$(globalCppFlags) $$(CppFlags_$1) $(ExternalLibraries)
	 
$$(testLibs_$$(directory_$1)) : build/$$(directory_$1)/test/%.cpp.dll:  $$(directory_$1)/test/%.cpp $$(lib_$$(directory_$1)) | $$(dependencies_$1) gtest.dll
	@mkdir -p $$(@D)
	g++ -shared $$^ $$| -o $$@ $$(globalCppFlags) $$(CppFlags_$1)  $$(SelfIncludes_$1) $$(dependencyIncludes_$1) $(testIncludes)

selfTestTarget_$$(directory_$1): 
	@echo "directory_$1             $$(directory_$1)" 
	@echo "files_$$(directory_$1)   $$(files_$$(directory_$1))" 
	@echo "sources_$$(directory_$1) $$(sources_$$(directory_$1))"
	@echo "tests_$$(directory_$1)   $$(tests_$$(directory_$1))"
	@echo "lib_$$(directory_$1)     $$(lib_$$(directory_$1))"
	@echo "SelfIncludes_$1          $$(SelfIncludes_$1)"
	@echo "dependencies_$1          $$(dependencies_$1)"
	@echo "dependencyIncludes_$1    $$(dependencyIncludes_$1)"
	@echo

main.exe: | $$(lib_$$(directory_$1))
test.exe: | $$(testLibs_$$(directory_$1)) 
selfTest: selfTestTarget_$$(directory_$1)  
endef
