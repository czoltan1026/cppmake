

gtestFiles   := $(shell find cppmake/test/ -name "*.cpp" -a -not -name *testMain.cpp)
gtestObjects := $(addprefix build/,$(gtestFiles)) 

gtestObjects := $(addsuffix .o,$(gtestObjects))
 
 
build/cppmake/test/%.cpp.o : cppmake/test/%.cpp
	@mkdir -p $(@D)  
	g++ -c $< -o $@  $(globalCppFlags) -Icppmake/test

gtest.dll : $(gtestObjects)
	@mkdir -p $(@D)  
	g++ -shared $^ -o $@  $(globalCppFlags) 