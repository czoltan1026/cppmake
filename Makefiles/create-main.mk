define create-main

directory_$1          := $1

SelfIncludes_$1       := $$(addprefix -I,$$(directory_$1))
dependencyIncludes_$1 := $$(addprefix -I,$2)

dependencies_$1 := $$(addsuffix .dll,$2)
dependencies_$1 := $$(subst src/,,$$(dependencies_$1))

swTestSource_$1            := $$(shell find swTest -name "*.cpp")
swTestObject_$1            := $$(addsuffix .o,$$(swTestSource_$1))
swTestObject_$1            := $$(addprefix build/,$$(swTestObject_$1))
swTestLib_$1               := $$(addsuffix .dll,$$(swTestObject_$1))

-include build/src/main.o.d

build/main/main.o : src/main/main.cpp 
	@mkdir -p $$(@D)  
	g++ -c $$<  -o $$@ $$(globalCppFlags) $$(SelfIncludes_$1) $$(dependencyIncludes_$1) $$(CppFlags_$1) -MMD -MF $$@.d

main.exe : build/main/main.o  | $$(dependencies_$1)
	@mkdir -p $$(@D)  
	g++ $$^ $$| $(ExternalLibraries) -o $$@  $$(globalCppFlags)

test.exe : |$$(swTestLib_$1)

$$(swTestObject_$1) : build/%.cpp.o : %.cpp
	@mkdir -p $$(@D)
	g++ -c $$< -o $$@ $$(globalCppFlags) $$(CppFlags_$1) $$(testIncludes) $$(SelfIncludes_$1) $$(dependencyIncludes_$1) -MMD -MF $$@.d

$$(swTestLib_$1)    : build/%.cpp.o.dll : build/%.cpp.o | $$(dependencies_$1) gtest.dll
	@mkdir -p $$(@D)  
	g++ -shared  $$<  $$| -o $$@ $$(globalCppFlags) $$(CppFlags_$1) $$(SelfIncludes_$1) $$(dependencyIncludes_$1) -MMD -MF $$@.d


endef


