#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include <filesystem>

extern "C" void* LoadLibraryA(const char*) __attribute__((stdcall));

using namespace std;

vector<string> collectTestDlls() {

    vector<string> testDlls;

    for (const auto& file : std::filesystem::recursive_directory_iterator("build")) {

        if (filesystem::path(file).string().find(".dll") != string::npos) {
            testDlls.push_back(filesystem::absolute(file).string());
        }
    }

    return testDlls;
}



int main(int ac, char** av) {



	vector<string> tests = collectTestDlls();

	string oneDll;

    for (const auto& test : tests) {
        LoadLibraryA(test.c_str());
    }

    testing::InitGoogleTest(&ac, av);
    int result = RUN_ALL_TESTS();

    return result;

}
