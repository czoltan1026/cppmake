globalCppFlags  := -g  --std=c++17 -O0 -Iinclude  -Winvalid-pch $(CppFlags) 
testIncludes    := -Icppmake/test/


%.dll : external/lib/%.dll
	cp $< $@

libDlls := $(shell find external/lib/ -name "*.dll")
dlls    := $(subst external/lib/,,$(libDlls))


all : main.exe 
test: test.exe 
clean:
	rm -rf *.lib *.dll *.o *.exe  build/* 
	
newfile:
	find build/ -name "*cache.txt" -exec rm {} \; 
	
build/test/main.o : cppmake/test/testMain.cpp
	@mkdir -p $(@D)  
	g++ -c $< -o $@ $(globalCppFlags) -Icppmake/test

test.exe : build/test/main.o | gtest.dll
	@mkdir -p $(@D)  
	g++ $^ $| -o $@  $(globalCppFlags)
	
include cppmake/Makefiles/create-module.mk
include cppmake/Makefiles/create-main.mk
include cppmake/Makefiles/gtest.mk



 
	